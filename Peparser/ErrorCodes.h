#pragma once
#ifndef ERRORCODES_H
#define ERRORCODES_H

//Throw codes
#define ERROR_FILE_NOT_FOUND		0
#define ERROR_WRONG_MZ_SIGNATURE	1
#define ERROR_WRONG_PE_SIGNATURE	2
#define ERROR_IN_CALCULATION		3


static char ErrorMessage[][50] = { 
	"File not found",
	"Wrong MZ start signature", 
	"Wrong PE signature",
	"Error during convertion RVA to Raw offset",
};

#endif 