#include "PeFile.h"
#include "string.h"
#include "ErrorCodes.h"

PEFILE::PEFILE(char *_s) {
	int i = 0;
	path = new char[strlen(_s)+1];
	for (unsigned int i = 0; i < strlen(_s); i++) {
		path[i] = _s[i];
	}
	path[strlen(_s)] = 0;
}
PEFILE::~PEFILE() {
	delete path;
	delete [] SectionTable;
}

bool PEFILE::openFile(){
	fopen_s(&F, path, "rb");
	if (!bool(F)) { throw ERROR_FILE_NOT_FOUND; }
	return bool(F);
}
void PEFILE::closeFile(){
	if (F) {
		fclose(F);
	}
}

DWORD PEFILE::readDwordAt(DWORD position){
	DWORD tmp;
	BYTE t0,t1,t2,t3;
	fseek(F, position, 0);
	fscanf_s(F, "%c", &t0,1);
	fscanf_s(F, "%c", &t1,1);
	fscanf_s(F, "%c", &t2,1);
	fscanf_s(F, "%c", &t3,1);
	tmp = t3;
	tmp = (tmp << 8) + t2;
	tmp = (tmp << 8) + t1;
	tmp = (tmp << 8) + t0;
	return tmp;
}
WORD PEFILE::readWordAt(DWORD position) {
	BYTE tmp0,tmp1;
	WORD tmp;
	fseek(F, position, 0);
	fscanf_s(F, "%c", &tmp0,1);
	fscanf_s(F, "%c", &tmp1, 1);
	tmp = tmp1;
	tmp <<= 8;
	tmp += tmp0;
	return tmp;
}
BYTE PEFILE::readByteAt(DWORD position) {
	BYTE tmp;	
	fseek(F, position, 0);
	fscanf_s(F, "%c", &tmp,1);
	return tmp;
}

void PEFILE::readDosHeader() {
	WORD signature=readWordAt(0x0);
	if (signature != 0x5A4D) { throw ERROR_WRONG_MZ_SIGNATURE; }	
	PE = readDwordAt(0x3C);	
	if (readWordAt(PE) != 0x4550) { throw ERROR_WRONG_PE_SIGNATURE; }
	NumberOfSections = readWordAt(PE + 6);
	SizeOfOptionalHeader = readWordAt(PE + 0x14);
}
void PEFILE::readOptionalHeader() {
	BaseOfCode = readDwordAt(PE + 0x2C);
	ImageBase	= readDwordAt(PE + 0x34);
	SizeOfImage = readDwordAt(PE + 0x50);		
}
void PEFILE::readSectionTable() {
	SectionTable = new SectionEntity[NumberOfSections];
	fseek(F, PE + 0x18 + SizeOfOptionalHeader, 0);
	fread(SectionTable,sizeof(SectionEntity), NumberOfSections,F);		
}
void PEFILE::showSectionTable() {
	for (uint i = 0; i < NumberOfSections; i++) {
		printf("Name ");
		for (int j = 0; j < 8; j++){
			printf("%c", SectionTable[i].Name[j]);
		}
		printf(", ");

		printf("VASize %x, ", SectionTable[i].Misc.VirtualSize);
		printf("VAOffset %x, ", SectionTable[i].VirtualAddress);
		printf("SizeRaw %x, ", SectionTable[i].SizeOfRawData);
		printf("RawOffset %x\n", SectionTable[i].PointerToRawData);
	}
}
// --- PUBLIC functions ---
void PEFILE::MainAnalyse() {
	openFile();
	readDosHeader();
	readOptionalHeader();
	readSectionTable();
	closeFile();
}
DWORD PEFILE::RVA2Raw(DWORD x) {
	DWORD tmp = x;
	if (tmp < ImageBase) { throw ERROR_IN_CALCULATION; }
	tmp -= ImageBase;
	//What section is it?
	uint k = -1;
	for (uint i = 0; i < NumberOfSections; i++) {
		if (
			(tmp >= SectionTable[i].VirtualAddress) &&
			(tmp < SectionTable[i].VirtualAddress + SectionTable[i].Misc.VirtualSize)
			) {
			k = i;
		}	
	}
	if (k == -1) { throw ERROR_IN_CALCULATION; }

	if (tmp < SectionTable[k].VirtualAddress) { throw ERROR_IN_CALCULATION; }
	tmp -= SectionTable[k].VirtualAddress;
	if (tmp>=SectionTable[k].SizeOfRawData) { throw ERROR_IN_CALCULATION; }
	tmp += SectionTable[k].PointerToRawData;

	return tmp;
}
SectionEntity PEFILE::getExeSection(uint i) { 
	SectionEntity tmp;
	memcpy(&tmp,&(SectionTable[i]),sizeof(SectionEntity));
	return tmp;
};
