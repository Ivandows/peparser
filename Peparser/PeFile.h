#pragma once
#include <stdio.h>
#include "Types.h"
#include "SectionHeader.h"
#include "ErrorCodes.h"

class PEFILE {
private:
	char* path;
	FILE* F;
	SectionEntity* SectionTable;

	DWORD PE;	
	DWORD NumberOfSections;
	DWORD SizeOfOptionalHeader;

	DWORD BaseOfCode;
	DWORD ImageBase;
	DWORD SizeOfImage;

	bool openFile();
	void closeFile();	

	DWORD readDwordAt(DWORD position);
	WORD  readWordAt(DWORD position);
	BYTE  readByteAt(DWORD position);

	void readDosHeader();
	void readOptionalHeader();
	void readSectionTable();

public:
	PEFILE(char* _path);
	~PEFILE();
	void MainAnalyse();
	DWORD RVA2Raw(DWORD address);
	void showSectionTable();
	DWORD getImageBase() { return ImageBase; }
	DWORD getNumberOfSections() { return NumberOfSections; }
	SectionEntity getExeSection(uint i); 
};
