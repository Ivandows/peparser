#pragma once
#ifndef TYPES_H
#define TYPES_H
typedef unsigned int uint;
typedef unsigned __int8 BYTE;
typedef unsigned __int16 WORD;
typedef unsigned __int32 DWORD;
typedef unsigned __int64 QWORD;
#endif // !TYPES_H

